module.exports = function (RED) {
  const signalR = require("@microsoft/signalr")

  function SensorOnlineClientNode(n) {
    RED.nodes.createNode(this, n);

    function newEvent(payload) {
      node.send({ payload: payload })
    }

    let url = null;
    if (n.url != null || n.url != "")
      url = n.url + "/events";

    var node = this;
    node.url = url;
    node.token = n.token;

    function tryConnect() {
      if (node.url == null || node.token == null || node.token == "" || node.url == "")
        return;

      var hub = new signalR.HubConnectionBuilder()
        .withUrl(node.url, {
          skipNegotiation: true,
          transport: signalR.HttpTransportType.WebSockets,
          accessTokenFactory: () => node.token
        })
        .configureLogging(signalR.LogLevel.Information)
        .withAutomaticReconnect()
        .build();

      node.hub = hub;
      node.hub.on("BroadcastEvent", newEvent)
      try {
        node.hub.start();
      } catch (error) {
        console.error("Could not connect", error)
      }
    }

    function disconnect() {
      if (node.hub != null) {
        node.hub.stop().then();
        console.log("disconnected")
      }

      console.log("Im here now closing...")
    }

    tryConnect();

    node.on("close", disconnect)
  }

  RED.nodes.registerType("sensor-online-client", SensorOnlineClientNode)
};
